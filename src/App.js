import './App.css';
import Users from './components/Users';

function App() {
  return (
    <div className="App container-fluid" style={{background:'black', height:'100vh'}}>
      <Users/>
    </div>
  );
}

export default App;
