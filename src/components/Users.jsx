import axios from 'axios';
import React from 'react'
// import Search from './Search';
import { useEffect, useState } from 'react'

export default function Users() {
    //sEARCH
    const [search, setSearch] = useState("");
    const handleSearchChange = (e) => {
        setSearch(e.target.value)
        console.log(search);
    }
    //API
    const [data, setData] = useState([]);
    const [errors, setErrors] = useState(null);
    const axiosData = ()=>{
        axios.get("https://jsonplaceholder.typicode.com/users")
        .then( res=> setData(res.data))
        .catch (err => setErrors(err))
    }
    console.log(data);
    
    useEffect(() => {
        axiosData()
    }, []);

  return (
    <div className='container d-flex flex-column justify-content-center align-items-center'>
            <div>
                <input type="text" className='mt-3' style={{width: 500, borderRadius:6}} onChange={handleSearchChange} />
            </div>

        <div>
        {data.filter((items) =>{
            return items.name.toLowerCase().includes(search.toLowerCase())
        }).map(items => (
            <div key={items.id} className="bg-body shadow-lg mt-5 mb-5" style={{width:300, borderRadius:14}}>
                <p> Name : {items.name} </p>
                <p> Username : {items.username} </p>
                <p> Email : {items.email} </p>
                <p> Street : {items.address.street} </p>
            </div>
        ))}
        </div>
    </div>
  )
}
